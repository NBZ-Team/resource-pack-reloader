package com.notboringzone.tools.resourcepackreloader;

import org.zeroturnaround.zip.ZipUtil;

import java.nio.file.Path;


class ResourcePackZipper {
    
    private ResourcePackZipper() {}

    
    static void createZip(Path resourcesFolder, Path resourcePackPath) {
        ZipUtil.pack(resourcesFolder.toFile(), resourcePackPath.toFile());
    }
}
