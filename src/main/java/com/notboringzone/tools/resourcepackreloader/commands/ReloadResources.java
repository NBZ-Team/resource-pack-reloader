package com.notboringzone.tools.resourcepackreloader.commands;

import com.google.inject.Inject;
import com.notboringzone.tools.resourcepackreloader.ResourcePackReloadUtils;
import com.notboringzone.tools.resourcepackreloader.ResourcePackReloaderPlugin;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;


public class ReloadResources implements CommandExecutor {
    private ResourcePackReloaderPlugin plugin;


    @Inject
    public ReloadResources(ResourcePackReloaderPlugin plugin) {
        this.plugin = plugin;
    }
    

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        ResourcePackReloadUtils.reloadResourcesForAllPlayers(
                sender.getServer().getOnlinePlayers(),
                plugin.getResourcesPath(),
                plugin.getResourcePackPath(),
                plugin.getResourcePackUrl());
        sender.sendMessage("Reloading Resources done!");
        return true;
    }
}
