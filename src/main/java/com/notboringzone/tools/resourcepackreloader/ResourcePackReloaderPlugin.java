package com.notboringzone.tools.resourcepackreloader;

import com.google.inject.Guice;
import com.google.inject.Inject;
import com.google.inject.Injector;
import com.notboringzone.tools.resourcepackreloader.commands.ReloadResources;
import com.notboringzone.tools.resourcepackreloader.config.Settings;
import com.notboringzone.tools.resourcepackreloader.config.SettingsRepository;
import com.notboringzone.tools.resourcepackreloader.guice.MainModule;
import com.notboringzone.tools.resourcepackreloader.listeners.PlayerJoin;
import org.bukkit.plugin.java.JavaPlugin;

import java.nio.file.Path;
import java.nio.file.Paths;


public final class ResourcePackReloaderPlugin extends JavaPlugin {
    private final Path RESOURCE_PACK_PATH = Paths.get("resource-pack.zip");
    private final static String RESOURCE_PACK_ENDPOINT_NAME = "resource-pack";
    
    
    @Inject
    private SettingsRepository settingsRepository;
    

    @Override
    public void onEnable() {
        Injector injector = Guice.createInjector(new MainModule(this));
        injector.injectMembers(this);
        getCommand("reload-resources").setExecutor(injector.getInstance(ReloadResources.class));
        getServer().getPluginManager().registerEvents(injector.getInstance(PlayerJoin.class), this);
        
        saveDefaultConfig();
        ResourcePackServer.start(getResourcePackPath(), settingsRepository.getSettings().getPort());
    }

    public Path getResourcePackPath() {
        return RESOURCE_PACK_PATH;
    }

    static String getResourcePackEndpointName() {
        return RESOURCE_PACK_ENDPOINT_NAME;
    }

    public String getResourcePackUrl() {
        return "http://localhost:" + settingsRepository.getSettings().getPort() + "/" + getResourcePackEndpointName();
    }

    public Path getResourcesPath() {
        return settingsRepository.getSettings().getResourcesPath();
    }
}
