package com.notboringzone.tools.resourcepackreloader.listeners;

import com.google.inject.Inject;
import com.notboringzone.tools.resourcepackreloader.ResourcePackReloadUtils;
import com.notboringzone.tools.resourcepackreloader.ResourcePackReloaderPlugin;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;


public class PlayerJoin implements Listener {
    private ResourcePackReloaderPlugin plugin;

    
    @Inject
    public PlayerJoin(ResourcePackReloaderPlugin plugin) {
        this.plugin = plugin;
    }
    

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        ResourcePackReloadUtils.reloadResources(
                event.getPlayer(),
                plugin.getResourcesPath(),
                plugin.getResourcePackPath(),
                plugin.getResourcePackUrl());    
    }
}
