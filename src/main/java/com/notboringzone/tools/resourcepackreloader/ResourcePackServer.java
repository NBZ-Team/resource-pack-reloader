package com.notboringzone.tools.resourcepackreloader;


import spark.Spark;

import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Path;

import static spark.Spark.get;
import static spark.Spark.port;


class ResourcePackServer {
    
    private ResourcePackServer() { }
    
    
    static void start(Path resourcePackPath, int port) {
        port(port);
        String endpointName = ResourcePackReloaderPlugin.getResourcePackEndpointName();

        Spark.before((req, res) -> {
            String path = req.pathInfo();
            if (path.equals("/") || path.equals("")) return;
            if (path.endsWith("/"))
                res.redirect(path.substring(0, path.length() - 1));
        });

        get("/", (request, response) -> "A Minecraft resource pack server!");

        get("/" + endpointName + "/*", (request, response) -> {
            response.raw().setHeader("Content-Disposition", "attachment; filename=" + resourcePackPath.getFileName());
            try {
                OutputStream outputStream = response.raw().getOutputStream();
                Files.copy(resourcePackPath, outputStream);
                outputStream.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        });
    }
}
