package com.notboringzone.tools.resourcepackreloader.guice;

import com.google.inject.AbstractModule;
import com.notboringzone.tools.resourcepackreloader.ResourcePackReloaderPlugin;
import org.bukkit.plugin.Plugin;


public class MainModule extends AbstractModule {
    private final ResourcePackReloaderPlugin plugin;
    

    public MainModule(ResourcePackReloaderPlugin plugin) {
        this.plugin = plugin;
    }

    @Override
    protected void configure() {
        bind(Plugin.class).toInstance(plugin);
        bind(ResourcePackReloaderPlugin.class).toInstance(plugin);
    }
}
