package com.notboringzone.tools.resourcepackreloader;

import org.bukkit.entity.Player;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Collection;


public class ResourcePackReloadUtils {
    
    public static void reloadResources(Player player,
                                       Path resourcesPath,
                                       Path resourcePackPath,
                                       String resourcePackUrl) {
        
        ResourcePackZipper.createZip(resourcesPath, resourcePackPath);
        String resourcePackMd5 = getResourcePackMd5(resourcePackPath);

        player.setResourcePack(resourcePackUrl + "/" + resourcePackMd5, resourcePackMd5);
    }

    public static void reloadResourcesForAllPlayers(Collection<? extends Player> onlinePlayers,
                                                    Path resourcesPath,
                                                    Path resourcePackPath,
                                                    String resourcePackUrl) {
        
        ResourcePackZipper.createZip(resourcesPath, resourcePackPath);
        String resourcePackMd5 = getResourcePackMd5(resourcePackPath);

        for (Player player : onlinePlayers) {
            player.setResourcePack(resourcePackUrl + "/" + resourcePackMd5, resourcePackMd5);
        }
    }

    private static String getResourcePackMd5(Path resourcePackPath) {
        String md5 = "";
        try (InputStream is = Files.newInputStream(resourcePackPath)) {
            md5 = org.apache.commons.codec.digest.DigestUtils.md5Hex(is);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return md5;
    }
}
