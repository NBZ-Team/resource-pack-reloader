package com.notboringzone.tools.resourcepackreloader.config;


import com.google.inject.Inject;
import com.google.inject.Singleton;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.plugin.Plugin;

import java.nio.file.Path;
import java.nio.file.Paths;


@Singleton
public class SettingsRepository {
    private final Plugin plugin;
    private final Settings settings;
    private static final String SERVER_ROOT_PATH_KEY = "serverRootPath";
    private static final String RESOURCES_PATH_KEY = "resourcesPath";
    private static final String PORT_KEY = "port";

    
    @Inject
    public SettingsRepository(Plugin plugin, Settings settings) {
        this.plugin = plugin;
        this.settings = settings;
    }
    

    public Settings getSettings() {
        loadSettings();
        return settings;
    }

    private void loadSettings() {
        FileConfiguration config = plugin.getConfig();
        String serverRootPath = config.getString(SERVER_ROOT_PATH_KEY);
        String resourcePackFolderPath = config.getString(RESOURCES_PATH_KEY);
        int port = config.getInt(PORT_KEY);
        settings.setServerRootPath(getPathOrNull(serverRootPath));
        settings.setResourcesPath(getPathOrNull(resourcePackFolderPath));
        settings.setPort(port);
    }

    private Path getPathOrNull(String pathAsString) {
        return pathAsString == null ? null : Paths.get(pathAsString);
    }
}
