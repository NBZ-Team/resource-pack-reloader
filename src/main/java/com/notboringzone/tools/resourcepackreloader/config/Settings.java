package com.notboringzone.tools.resourcepackreloader.config;

import com.google.inject.Singleton;

import java.nio.file.Path;


@Singleton
public class Settings {
    private Path serverRootPath;
    private Path resourcesPath;
    private int port;


    public Path getServerRootPath() {
        return serverRootPath;
    }

    void setServerRootPath(Path serverRootPath) {
        this.serverRootPath = serverRootPath;
    }

    public Path getResourcesPath() {
        return resourcesPath;
    }

    void setResourcesPath(Path resourcesPath) {
        this.resourcesPath = resourcesPath;
    }

    public int getPort() {
        return port;
    }

    void setPort(int port) {
        this.port = port;
    }
}
